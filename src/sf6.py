#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import sys
import serial
import time
import rospy

import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_rtu
from std_msgs.msg import Float32

PORT = '/dev/ttyUSB0'

def main():
    """main"""
    rospy.init_node("ucr_sf6")
    pub = rospy.Publisher('sf6_msg', Float32, queue_size=10)

    logger = modbus_tk.utils.create_logger("console")
    try:
        #connect to the slave
        master = modbus_rtu.RtuMaster(serial.Serial(port=PORT, baudrate=9600, bytesize=8, parity='N', stopbits=1, xonxoff=0))
        master.set_timeout(3.0)
        master.set_verbose(False)
        print("connected")
        # Read SF6
        while not rospy.is_shutdown():
            sf6 = master.execute(1, cst.READ_HOLDING_REGISTERS, 0x0006, 1)[0]
            print("SF6: {}ppm".format(sf6))
            pub.publish(sf6)
            time.sleep(1)
        
    except modbus_tk.modbus.ModbusError as exc:
        logger.error("%s- Code=%d", exc, exc.get_exception_code())

if __name__ == "__main__":
        main()
