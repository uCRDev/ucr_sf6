#!/usr/bin/env python3
# -*- coding:utf-8 -*-

import sys
import serial
import time
import rospy

import modbus_tk
import modbus_tk.defines as cst
from modbus_tk import modbus_rtu
from std_msgs.msg import Float32

PORT = '/dev/ttyUSB0'

def main():
    """main"""
    logger = modbus_tk.utils.create_logger("console")
    try:
        #connect to the slave
        master = modbus_rtu.RtuMaster(serial.Serial(port=PORT, baudrate=9600, bytesize=8, parity='N', stopbits=1, xonxoff=0))
        master.set_timeout(3.0)
        master.set_verbose(True)
        print("connected")
        # Change SF6 reading register address
        master.execute(0xFD, cst.WRITE_SINGLE_REGISTER, 0x0100, output_value=1)
        time.sleep(1)
        addr = master.execute(0xFD, cst.READ_HOLDING_REGISTERS, 0x0100, 1)[0]
        print("address: {}".format(addr));
        
    except modbus_tk.modbus.ModbusError as exc:
        logger.error("%s- Code=%d", exc, exc.get_exception_code())

if __name__ == "__main__":
        main()
